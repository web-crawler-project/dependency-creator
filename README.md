# Dependencies Creator lib

This library pulls dependencies and sub-dependencies by looking for packages from alpine package.

## Dependencies

 - Python3.x
   - requests
   - bs4

## Using

the library so function name is `dependency_creator()` and takes 2 parameters.

 - package_name : the package name create dependencies tree 
 - repo_type : there is three type alpine repository, `main`, `community`, `testing` , please look alpine package site https://pkgs.alpinelinux.org/packages (not every package is in every repository type)

**for example :**
```
dependency_creator("gcc","main")

```
