import requests
from bs4 import BeautifulSoup

dependencies_json = ""
dependencies_all = []

class dependency_creator():

    def __init__(self, package_name, repo_type):

        global dependencies_json
        global dependencies_all

        url = f'https://pkgs.alpinelinux.org/package/edge/{repo_type}/x86_64/{package_name}'
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
        response = requests.get(url, headers=headers)
        html = response.text
        recursive = True
        dependencies = []

        soup = BeautifulSoup(html, 'html.parser')

        for div in soup.find_all("div",class_="pure-u-1 pure-u-lg-6-24 multi-fields"):
            if( str(div.find("summary").text).split()[0] == "Depends"):
                for ul in div.find_all("ul"):
                    for li in ul.find_all("li"):
                        recursive=True
                        for i in dependencies_all:
                            if( i == str(li.text)):
                                recursive=False
                                continue
                        if( recursive == True ):
                            print(str(li.text))
                            dependencies_all.append(str(li.text))
                            dependency_creator(str(li.text), repo_type)
            else:
                break

        dependencies_json = dependencies_json + package_name + str(dependencies)+","
#        print(dependencies_json)


try:
    dependency_creator("gnome-music", "community")
except KeyboardInterrupt:
    print("exiting...")
print("done !!!")

